from src.quiz import Quiz


class Teacher:
    def create_quiz(self, class_=None, questions=None):
        return Quiz(class_=class_, questions=questions, semester='2000.1')

    def assign_quiz(self, quiz, student):
        quiz.assign_to(student)
