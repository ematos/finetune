class Class:
    def __init__(self, teacher=None, students=None):
        if not teacher:
            raise AssertionError('It is mandatory to provide a teacher')

        if not students:
            raise AssertionError(
                'It is mandatory to provide at least one student')

        self.students = students
        self.teacher = teacher
