class Quiz:
    def __init__(self, class_=None, questions=None, semester=None):
        self.class_ = class_

        if not questions:
            raise AssertionError('A quiz must have at least one question')

        if not semester:
            raise AssertionError('A quiz must be assigned to a semester')

        self.questions = questions
        self.semester = semester

    def assign_to(self, student):
        self.student = student
        self.student.assign(self)

    def finished(self):
        return all(q.answered for q in self.questions)

    @property
    def grade(self):
        total = len(self.questions)
        correct = len([q for q in self.questions if q.correct])

        return 10 * correct / total
