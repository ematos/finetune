class Student:
    def __init__(self):
        self.quizzes = set()

    def grade(self, semester):
        grades = [quiz.grade
                  for quiz in self.quizzes
                  if quiz.semester == semester]
        return 0 if not len(grades) else sum(grades)/len(grades)

    def assign(self, quiz):
        self.quizzes.add(quiz)

    def answer(self, choice):
        choice.check()
