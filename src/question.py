class Question:
    def __init__(self, choices=None):
        if not choices:
            raise AssertionError('A question must have several choices')

        self.choices = choices

    @property
    def answered(self):
        return any(c.checked for c in self.choices)

    @property
    def correct(self):
        choices = [choice for choice in self.choices if choice.checked]

        if choices and choices[0].correct:
            return True
        return False
