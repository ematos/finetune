class Choice:
    def __init__(self, correct=False):
        self.checked = False
        self.correct = correct

    def check(self):
        self.checked = True
