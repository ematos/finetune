from unittest import TestCase
from src.teacher import Teacher
from src.student import Student
from src.class_ import Class


class ClassFormationTest(TestCase):
    def test_class_has_a_teacher(self):
        teacher = Teacher()
        class_ = Class(teacher=teacher, students=[Student()])

        self.assertEqual(teacher, class_.teacher)

    def test_class_must_have_a_teacher(self):
        self.assertRaises(AssertionError, Class)

    def test_with_no_students(self):
        self.assertRaises(AssertionError,
                          Class, teacher=Teacher(), students=[])

    def test_with_students(self):
        students = [Student(), Student()]
        class_ = Class(teacher=Teacher(), students=students)

        self.assertEqual(students, class_.students)
