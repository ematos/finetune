from unittest import TestCase
from src.quiz import Quiz
from src.question import Question
from src.choice import Choice


class QuizCreationTest(TestCase):
    def test_a_quiz_cannot_have_zero_questions(self):
        self.assertRaises(AssertionError, Quiz, semester='2000.1')

    def test_a_quiz_must_be_assigned_to_a_semester(self):
        self.assertRaises(AssertionError, Quiz,
                          questions=[Question(choices=[Choice()])])

    def test_a_quiz_can_have_one_question(self):
        questions = [Question(choices=[Choice()])]

        quiz = Quiz(questions=questions, semester='2000.1')

        self.assertEqual(quiz.questions, questions)

    def test_a_quiz_can_have_many_questions(self):
        questions = [Question(choices=[Choice()]),
                     Question(choices=[Choice()]),
                     Question(choices=[Choice()])]

        quiz = Quiz(questions=questions, semester='2001.2')

        self.assertEqual(quiz.questions, questions)
