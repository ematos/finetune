from unittest import TestCase
from src.quiz import Quiz
from src.question import Question
from src.choice import Choice
from src.student import Student


class QuizSolving(TestCase):
    def test_solve_entire_quiz(self):
        student = Student()
        choice_1, choice_2 = Choice(), Choice()
        question = Question(choices=[choice_1, choice_2])
        quiz = Quiz(questions=[question], semester='2000.1')
        quiz.assign_to(student)

        student.answer(choice_1)

        self.assertTrue(quiz.finished())

    def test_solve_quiz_partially(self):
        student = Student()
        choice_1, choice_2 = Choice(), Choice()
        question_1 = Question(choices=[choice_1])
        question_2 = Question(choices=[choice_2])
        quiz = Quiz(questions=[question_1, question_2], semester='2000.1')
        quiz.assign_to(student)

        student.answer(choice_1)

        self.assertFalse(quiz.finished())

    def test_grade_10_by_solving_quiz_correctly(self):
        student = Student()
        choice = Choice(correct=True)
        question = Question(choices=[choice])
        quiz = Quiz(questions=[question], semester='2000.1')
        quiz.assign_to(student)

        student.answer(choice)

        self.assertEqual(10, quiz.grade)

    def test_grade_5_by_solving_quiz_partially_correct(self):
        student = Student()
        choice_1, choice_2 = Choice(correct=True), Choice()
        question_1 = Question(choices=[choice_1])
        question_2 = Question(choices=[choice_2])
        quiz = Quiz(questions=[question_1, question_2], semester='2000.1')
        quiz.assign_to(student)

        student.answer(choice_1)

        self.assertEqual(5, quiz.grade)

    def test_grade_0_by_solving_quiz_incorrectly(self):
        student = Student()
        choice_1, choice_2 = Choice(correct=True), Choice(correct=False)
        question_1 = Question(choices=[choice_1, choice_2])
        quiz = Quiz(questions=[question_1], semester='2000.1')
        quiz.assign_to(student)

        student.answer(choice_2)

        self.assertEqual(0, quiz.grade)
