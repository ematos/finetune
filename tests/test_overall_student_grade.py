from unittest import TestCase
from src.question import Question
from src.choice import Choice
from src.student import Student
from src.quiz import Quiz


class OverallStudentGradeTest(TestCase):
    def test_student_answers_one_correct_quiz_over_semester(self):
        student = Student()
        choice = Choice(correct=True)
        question = Question(choices=[choice])
        quiz = Quiz(questions=[question], semester='2000.1')
        quiz.assign_to(student)

        student.answer(choice)

        self.assertEqual(10, student.grade(semester='2000.1'))

    def test_student_answers_partially_correct_quizzes_over_semester(self):
        student = Student()
        choice_1, choice_2 = Choice(correct=True), Choice()
        question_1 = Question(choices=[choice_1])
        question_2 = Question(choices=[choice_2])
        quiz = Quiz(questions=[question_1, question_2], semester='2000.1')
        quiz.assign_to(student)

        student.answer(choice_1)
        student.answer(choice_2)

        self.assertEqual(5, student.grade(semester='2000.1'))

    def test_student_answers_quizzes_over_several_semesters(self):
        student = Student()
        choice_1, choice_2 = Choice(correct=True), Choice(correct=False)

        question_1 = Question(choices=[choice_1])
        question_2 = Question(choices=[choice_2])

        quiz_1 = Quiz(questions=[question_1], semester='1999.2')
        quiz_2 = Quiz(questions=[question_2], semester='2000.1')

        quiz_1.assign_to(student)
        quiz_2.assign_to(student)

        student.answer(choice_1)
        student.answer(choice_2)

        self.assertEqual(10, student.grade(semester='1999.2'))
        self.assertEqual(0, student.grade(semester='2000.1'))
