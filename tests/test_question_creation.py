from unittest import TestCase
from src.choice import Choice
from src.question import Question


class QuestionCreationTest(TestCase):
    def test_a_quest_cannot_have_zero_choices(self):
        self.assertRaises(AssertionError, Question)

    def test_a_question_can_have_one_choice(self):
        choices = [Choice()]

        question = Question(choices=choices)

        self.assertEqual(question.choices, choices)

    def test_a_question_can_have_many_choice(self):
        choices = [Choice()]

        question = Question(choices=choices)

        self.assertEqual(question.choices, choices)
