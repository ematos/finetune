from unittest import TestCase
from src.teacher import Teacher
from src.student import Student
from src.class_ import Class
from src.question import Question
from src.choice import Choice


class TeacherDealingWithQuizzes(TestCase):
    def test_teacher_create_a_quiz(self):
        teacher = Teacher()
        class_ = Class(teacher=teacher, students=[Student()])

        quiz = teacher.create_quiz(class_=class_,
                                   questions=[Question(choices=[Choice()])])

        self.assertEqual(class_, quiz.class_)

    def test_assign_a_quiz(self):
        teacher = Teacher()
        student = Student()
        class_ = Class(teacher=teacher, students=[Student()])
        quiz = teacher.create_quiz(class_=class_,
                                   questions=[Question(choices=[Choice()])])

        teacher.assign_quiz(quiz, student)

        self.assertEqual(quiz.student, student)
